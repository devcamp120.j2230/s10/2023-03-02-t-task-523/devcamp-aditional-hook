import { useContext } from "react"
import { ExampleContext } from "./ExampleContext"

function ChildComponent2() {
    const valueFromContext = useContext(ExampleContext);
    return (
        <h1>{valueFromContext.secondValue}</h1>
    )
}

export default ChildComponent2